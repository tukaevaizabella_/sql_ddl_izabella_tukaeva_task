create schema if not exists books;

create table if not exists books.author 
( 
    author_id serial primary key,
    author_name text not null unique,
    record_ts timestamp default current_timestamp
);


create table if not exists books.book
( 
    book_id serial primary key,
    book_name text not null unique,
    book_year int check (book_year >= 2000),
    measured_value numeric check (measured_value >= 0),
    gender text check (gender in ('Male', 'Female', 'Other')),
    record_ts timestamp default current_timestamp,
    author_id int references books.author(author_id)
);




insert into books.author (author_name) values ('Author1'), ('Author2');


insert into books.book (book_name, book_year, measured_value, gender, author_id)
values ('Book1', 2005, 10.5, 'Male', 1),
       ('Book2', 2010, 20.0, 'Female', 2);


alter table books.book add constraint uk_gender unique (gender);


alter table books.book add constraint chk_record_ts check (record_ts is not null);


alter table books.author add constraint chk_record_ts check (record_ts is not null);